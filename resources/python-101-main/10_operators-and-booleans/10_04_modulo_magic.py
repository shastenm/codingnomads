# Use the modulo operator to confirm which of the values
# shown below are divisible by seven.

num_1 = 42
num_2 = 137
num_3 = 455
num_4 = 1997

if num_1 % 7 == 0:
    print("num_1", num_1, "is divisible by 7.")

if num_2 % 7 == 0:
    print("num_2", num_2, "is divisible by 7.")

if num_3 % 7 == 0:
    print("num_3", num_3, "is divisible by 7.")

if num_4 % 7 == 0:
    print("num_4", num_4, "is divisible by 7.")
