# Write a script that takes a string of words and a letter from the user.
# Find the index of first occurrence of the letter in the string. For example:
#
# String input: hello world
# Letter input: o
# Result: 4

words = input("Give me a series of words: ")
words = words.lower()
letter = input("Give me a letter: ")
letter = letter.lower()

flag = 0
for i in range(len(words)):
    if words[i] == letter:
        flag == 1
        break

if flag == 0:
    print(letter, "is not a character in this series of words. ")
else:
    print("The first occurence of", letter, "is found", i + 1, "in the string.")

