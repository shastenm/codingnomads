# Use the interpreter to calculate how many seconds are in a year.
# Then write the code in this script file below this line.

days = 365
hours = 24
minutes = 60
seconds = 60

secondsInYear = days * hours * minutes * seconds

print(secondsInYear)
