import random
import sys

def main():
    num = random.randint(1,20)
    guess = None
    count = 5
    while num != guess and count > 0:
        guess = int(input("Guess a number: "))
        count -= 1
        if guess == num:
            print("You won!")
            break
        else:
           print("Try again.")
    quit()
    if not count:
        print("You ran out of tries. Thanks for playing.")
     
   

def quit():
    quit = input("Do you want to play again (Y/N)? ")
    quit = quit.upper()
    if quit == "Y":
        main()
    else:
        sys.exit("Thanks for playing.")

main()
