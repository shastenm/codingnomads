# Write the necessary code to print the BIG PYTHON string shown below.
# Research multi-line strings to make this easier for you :)
#
#     PPPP   Y     Y  TTTTTTTTT  H    H      O     N       N
#     P   P   Y   Y       T      H    H     O O    N N     N
#     P   P    Y Y        T      H    H    O   O   N  N    N
#     PPPP      Y         T      HHHHHH    O   O   N   N   N
#     P         Y         T      H    H    O   O   N    N  N
#     P         Y         T      H    H     O O    N     N N
#     P         Y         T      H    H      O     N       N

py_string = ('PPPP\tY     Y\t   TTTTTTTTT\tH\tH\tO\tN\tN\n'
            'P   P\t Y   Y\t       T\tH\tH      O O\tN N\tN\n'
            'P   P\t  Y Y\t       T\tH\tH     O   O\tN  N\tN\n'
            'PPPP\t   Y\t       T\tHHHHHHHHH     O   O\tN   N   N\n'
            'P\t   Y\t       T\tH\tH     O   O\tN    N  N\n'
            'P\t   Y\t       T\tH\tH      O O\tN     N N\n'
            'P\t   Y\t       T\tH\tH       O   \tN\tN\n'
)

print(py_string)

