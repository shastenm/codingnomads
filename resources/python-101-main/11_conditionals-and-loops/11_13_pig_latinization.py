import sys
# Fetch the text of the CodingNomads collaborative story from:
# https://raw.githubusercontent.com/CodingNomads/collaborative-story/master/story.txt
# Ask user to enter a word
def main():
    word = input("Enter a word: ")
    word = word.lower()
    length = word[1:]
    first_letter = word[0]

    # Take the first consonant of a word, move it to the end of the word and add "ay"
    # If a word begins with a vowel, you just add "way" to the end.
    while first_letter != 'a' and first_letter != 'e' and first_letter != 'i' and first_letter != 'o' and first_letter != 'u':
        print(length + first_letter + "ay")
        break
    else:
        print(word + "way")
    quit()

def quit():

    quit = input("Do you want to quit (Y/N)? ")
    quit = quit.upper()
    while quit == "Y":
        sys.exit("Thanks for playing.")
    else:
        main()

main()
