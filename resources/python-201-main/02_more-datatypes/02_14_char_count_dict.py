# Write a script that takes a text input from the user
# and creates a dictionary that maps the letters in the string
# to the number of times they occur. For example:
#
# user_input = "hello"
# result = {"h": 1, "e": 1, "l": 2, "o": 1}

n = 1
freq = {}
word = input("Word: ")
#for char in word :
#    result = {char: n}
for char in word:
    if char in freq:
        freq[char] += 1
    else:
        freq[char] = 1

print(freq)