# Adapt your Generator expression from the previous exercise:
# Don't print anything, and run a floor division by 1111 on it.
# What numbers do you get?
gen = [nums // 1111 for nums in range(1, 1000000)]
for i in gen:
    print(i)
