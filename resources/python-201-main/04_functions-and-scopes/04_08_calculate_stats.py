# Write a function named `stats()` that takes in a list of numbers
# and finds the maximum, minimum, average and sum of the numbers.
# Print these values to the console you call the function.

def stats():
    start = 1
    example_list = [1, 2, 3, 4, 5, 6, 7]
    index_length = len(example_list)
    min_list = min(example_list)
    max_list = max(example_list)

    for i in range(index_length):
        i += 1
        start = start + i
        sum_of_numbers = start
        average = start // len(example_list)

    print("Min:", min_list, "Max:", max_list, "AVG:", average, "Total Sum:", sum_of_numbers)
stats()

