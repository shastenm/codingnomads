# Combine the `greet()` function that you developed in the course materials
# with the `write_letter()` function from the previous exercise.
# Write both functions in this script and call `greet()` within `write_letter()`
# to let `greet()` take care of creating the greeting string.

def hello():
    name = input("What's your name? ")       

    def write_letter(name):
        text = print(f"""               Hi {name},\n
                I just wanted to say how 
                how nice it was to see you the other night
                at the BBQ. We will be in touch shortly {name}.\n
                Respectfully, Bill""")

    write_letter(name)

hello()