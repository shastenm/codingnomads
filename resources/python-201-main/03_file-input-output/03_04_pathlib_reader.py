# Refactor your file counter script to use `pathlib` also for
# reading and writing to your CSV file. Make sure to handle the
# path in a way so that you can run the script from anywhere.

from pathlib import Path

data_path = Path("/home/tuxer/Desktop")

with open(data_path.joinpath("input.txt"), "r") as file_in:
    print(file_in.read())