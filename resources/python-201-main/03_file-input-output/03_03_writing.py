# Write a script that reads in the contents of `words.txt`
# and writes the contents in reverse to a new file `words_reverse.txt`.
words = []

with open("words.txt", "r") as file_input:
	for word in file_input.readlines():
		word = word.strip()
		words.append(word)

rev_words = []

for w in words:
	rev_words.append(w)
	rev_words = rev_words[::-1]

with open("reverse_words.txt", "w") as file_out:
	file_out.write(f"{rev_words}")

