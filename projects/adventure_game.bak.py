# Save the user input options you allow e.g. in a set that you can check against when your user makes a choice.
# Create an inventory for your player, where they can add and remove items.
# Players should be able to collect items they find in rooms and add them to their inventory.
# If they lose a fight against the dragon, then they should lose their inventory items.
# Add more rooms to your game and allow your player to explore.
# Some rooms can be empty, others can contain items, and yet others can contain an opponent.
# Implement some logic that decides whether or not your player can beat the opponent depending on what items they have in their inventory
# Use the random module to add a multiplier to your battles, similar to a dice roll in a real game. This pseudo-random element can have an effect on whether your player wins or loses when battling an opponent.
# Once you've got a working version done, show it again to your friends and family and let them give it another spin. Feel free to share a link to your game on the forum in the thread on Command-Line Games.


import sys
import random as ran

def player_items():
    pass

def dice_roll():
    pass 

def player_items():
    player_items = []

def enemies():
    pass

def room(player_data):
    choice = input("What room would you like to enter (N)orth, (S)outh, (E)ast, or (W)est? ").upper()
    
    if choice == "N":
        north_room(player_data) 
    elif choice == "S": 
        south_room(player_data)
    elif choice == "W":
        west_room(player_data)
    else:
        east_room(player_data) 

def east_room(player_data):
    pass

def west_room(player_data):
    player_combat = input("There is a dragon. Do you want to (L)eave or (S)tay and fight? ").upper()
    player_combat == True
    weapons(player_data)

def north_room(player_data):
    pass

def south_room(player_data):
    pass

def staff(player_data):
    pass
#if player has a staff, they will get swiped once before killing the dragon. 

def sword(player_data):
    print("Congrats, you just defeated the dragon.")
    quit()

def no_weapon(player_data):
    dragon_swipe = True
    health(player_data)


def lives(player_data):
    while player_data['lives'] > 1:
        player_data['lives'] -= 1    
        print(f"You just lost a life. You now have {player_data['lives']} lives left.")
        room(player_data)    
    else:
        print("You lose.")
        quit()

def health(player_data):
    player_health = 6
    player_health = int(player_health)
    while player_health > 2:
        player_health -= 2
        print(f"You just got swiped by the dragon. You now have {player_health} health.")
        player_combat = input("Do you want to (L)eave or (S)tay and fight? ").upper()
        if player_combat == "L":
            lives(player_data)
    else:
        lives(player_data)

def weapons(player_data):
    player_items = player_data['items']

    if not player_items:
        no_weapon(player_data)

    elif player_items[0] == "sword":
        sword(player_data)

    else:
        staff(player_data)
    
def quit():
    play_again = input("Game Over. Would you like to play again (Y/N?)").upper()
    if play_again == "Y":
        room()
    else:
        sys.exit("Thanks for playing.")

def main():
    player_lives = 3
    player_score = 0
    player_items = []

    player_data = {'lives': player_lives, 'score': player_score, 'items': player_items}
    room(player_data)

main()
