
# Ask the player for their name.
player = input("What's Your Name? ")

# Display a message that greets them and introduces them to the game world.
print("Hi", player, "Welcome!")
greeting = input("Would You Like To Play A Game? ")
look_around = ""
if greeting == "Y" or "y":
    print("Let's Play A Game! ")

""" function enter_room gives the player the opportunity to choose between two doors,
    if player chooses the Left door, then they can choose to 'stay' and 'look around',
    or to leave and choose which door they want to enter in. If player 'looks around',
    they find a hidden sword. They then have the choice to take it or not. Regardless
    of whether or not they take the sword, they can then choose which room where they
    find out their fate.
"""

def room():
    choice = input("Which door would you like to enter? (L)eft or (Right)? ")
    choice = choice.upper()
    if choice == "L":
        left_room()
    else:
        right_room()

def left_room():
    empty = input("It looks rather empty? Would you like to check out what's in the door on the (R)ight, or would you rather (S)tay and have a look around? ")
    empty = empty.upper()
    if empty == "S":
        global look_around
        look_around = input("There's a sword that was hidden. Do you want to take it? (Y/N)? ")
        look_around = look_around.upper()
        if look_around == "Y":
            room()
        else:
            print("Player opted not to take the sword. You can always change your mind later.")
            room()

def right_room():
    dragon = input("There's a dragon. Do you want to (F)ight, or do you what to (R)un? ")
    dragon = dragon.upper()
    if dragon == ("F"):
        if look_around == "Y":
            with_sword()
        else:
            no_sword()
    else:
        room()

# This declares the player the  winner and gives the player the option to continue playing or quit.
def with_sword():
    print("You just cut the dragon's head off! You win!")
    play_again = input("Since you won, you can play again if you'd like. (Y/N) ")
    play_again = play_again.upper()
    if play_again == "Y":
        room()
    else:
        quit = input("Would you like to quit (Y/N)? ")
        quit = quit.upper()
        if quit == "Y":
            quit()
# This declares the player the loser and gives the player the option to continue playing or quit
def no_sword():
    print("You just became a big mac for the dragon. You lose! Game Over.")
    print("Would you like to play again (Y/N)? ")
    play_again = input("Since you lost, you can play again if you'd like. (Y/N) ")
    play_again = play_again.upper()
    if play_again == "Y":
        room()
    else:
        quit = input("Would you like to quit (Y/N)? ")
        quit = quit.upper()
        if quit == "Y":
            quit()

room()

exit
